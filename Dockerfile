FROM adoptopenjdk/openjdk11:alpine

WORKDIR /app

COPY ./target/sinister-0.0.1-SNAPSHOT.war /app/sinister-0.0.1-SNAPSHOT.war

EXPOSE 8080

ENTRYPOINT ["java","-jar","/app/sinister-0.0.1-SNAPSHOT.war"]