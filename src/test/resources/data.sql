DROP TABLE IF EXISTS contract;
CREATE TABLE contract (
    id              integer,
    clientid        integer,
    type            text
);
INSERT INTO contract(id, clientid, type) VALUES
    (1, 2, 'BIENS'),
    (2, 2, 'MALADIE'),
    (3, 1, 'PERTE_EXPLOITATION');


DROP TABLE IF EXISTS sinister;
CREATE TABLE sinister (
    id              int PRIMARY KEY auto_increment,
    type            text,
    status          text,
    description     text,
    contractid      int,
    cost            int,
    date            date
);
INSERT INTO sinister(id, type, status, description, contractid, cost, date) VALUES
(1, 'VOL', 'ACCEPTED', 'Vol de téléphone portable', 3, 250, '2019-05-09'),
(2, 'INCENDIE', 'ACCEPTED', 'Incendie criminel', 3, 250, '2019-05-09'),
(3, 'CATASTROPHES_NATURELLES', 'ACCEPTED', 'Tremblement de terre', 2, 6500, '2018-08-12'),
(4, 'DEGAT_DES_EAUX', 'DENIED', 'Fuites', 1, 300, '2019-05-09');
