package fr.nexworld.demo.service;

import fr.nexworld.demo.model.Sinister;
import fr.nexworld.demo.model.SinisterStatus;
import fr.nexworld.demo.model.SinisterType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class SinisterServiceTest {

    @Autowired
    SinisterService sinisterService;

    @Test
    public void testGetAllSinister() {
        List sinisterListList = sinisterService.findAll();
        assertNotNull("Sinister list returned was null", sinisterListList);
        assertFalse("Sinister list returned was empty", sinisterListList.isEmpty());
    }

    @Test
    public void testGetSinisterById() {
        Sinister sinister = sinisterService.findById((long) 2);

        assertNotNull("Sinister returned was null", sinister);
        assertEquals("INCENDIE", sinister.getType().toString());
        assertEquals("ACCEPTED", sinister.getStatus().toString());
        assertEquals("Incendie criminel", sinister.getDescription());
        assertEquals(3, (long) sinister.getContract().getId());
        assertEquals(250, (long) sinister.getCost());
    }

    @Test
    public void testGetClientByIdNotFound() {
        Sinister sinistre = sinisterService.findById((long) 99);
        assertNull("Sinister returned was not null", sinistre);
    }

    @Test
    public void testAddSinister() {
        Sinister sinisterToAdd = new Sinister();
        sinisterToAdd.setType(SinisterType.VOL);
        sinisterToAdd.setStatus(SinisterStatus.SUBMITTED);
        sinisterToAdd.setDescription("Cambriollage d'appartement");
        sinisterToAdd.setCost((long) 200);
        sinisterToAdd.setDate(new Date());
        sinisterService.addSinister(sinisterToAdd);
        Sinister sinisterAdded = sinisterService.findById(sinisterToAdd.getId());
        assertNotNull("Sinister returned was null", sinisterAdded);
        assertEquals(SinisterType.VOL, sinisterAdded.getType());
        assertEquals(SinisterStatus.SUBMITTED, sinisterAdded.getStatus());
        assertEquals("Cambriollage d'appartement", sinisterAdded.getDescription());
        assertEquals(200, (long) sinisterAdded.getCost());
    }

    @Test
    public void testDeleteClient() {
        Sinister sinisterToDelete = sinisterService.findById((long) 4);
        assertNotNull("Initial sinister was not found", sinisterToDelete);
        sinisterService.deleteSinister(sinisterToDelete);
        Sinister sinisterDeleted = sinisterService.findById(sinisterToDelete.getId());
        assertNull("Sinister was not deleted", sinisterDeleted);
    }

}
