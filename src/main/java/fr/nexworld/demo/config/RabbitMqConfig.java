package fr.nexworld.demo.config;

import fr.nexworld.demo.service.rabbitmq.MessageProducer;
import fr.nexworld.demo.service.rabbitmq.MessageReceiver;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqConfig {

    @Bean
    public FanoutExchange fanoutCreate() {
        return new FanoutExchange("sinister.created");
    }

    @Bean
    public FanoutExchange fanoutUpdate() {
        return new FanoutExchange("sinister.updated");
    }

    @Bean
    public FanoutExchange fanoutDelete() {
        return new FanoutExchange("sinister.deleted");
    }

    @Bean
    public FanoutExchange fanoutContractCreate() {
        return new FanoutExchange("contract.created");
    }

    @Bean
    public FanoutExchange fanoutContractUpdate() {
        return new FanoutExchange("contract.updated");
    }

    @Bean
    public FanoutExchange fanoutContractDelete() {
        return new FanoutExchange("contract.deleted");
    }

    @Bean
    public Queue autoCreateContractQueue() {
        return new Queue("contract.created.service_sinister");
    }

    @Bean
    public Queue autoUpdateContractQueue() {
        return new Queue("contract.updated.service_sinister");
    }

    @Bean
    public Queue autoDeleteContractQueue() {
        return new Queue("contract.deleted.service_sinister");
    }

    @Bean
    public Binding bindingUpdateContract(FanoutExchange fanoutContractCreate, Queue autoCreateContractQueue) {
        return BindingBuilder.bind(autoCreateContractQueue).to(fanoutContractCreate);
    }

    @Bean
    public Binding bindingCreateContract(FanoutExchange fanoutContractUpdate, Queue autoUpdateContractQueue) {
        return BindingBuilder.bind(autoUpdateContractQueue).to(fanoutContractUpdate);
    }

    @Bean
    public Binding bindingDeleteContract(FanoutExchange fanoutContractDelete, Queue autoDeleteContractQueue) {
        return BindingBuilder.bind(autoDeleteContractQueue).to(fanoutContractDelete);
    }

    @Bean
    public MessageReceiver receiver() {
        return new MessageReceiver();
    }

    @Bean
    public MessageProducer sender() {
        return new MessageProducer();
    }
}

