package fr.nexworld.demo.dao;

import fr.nexworld.demo.model.Contract;
import org.springframework.stereotype.Repository;

@Repository
public class ContractDAOImpl extends GenericDAOImpl<Contract> implements ContractDAO {

}
