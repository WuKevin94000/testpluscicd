package fr.nexworld.demo.dao;

import java.io.Serializable;
import java.util.List;

public interface GenericDAO<T extends Serializable> {

    void create(final T entity);

    List<T> findAll();

    T getById(final Long id);

    void update(final T entity);

    void delete(final T entity);

    List<T> findAllOrderByAsc(final String column);

}
