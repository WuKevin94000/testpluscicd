package fr.nexworld.demo.dao;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

public abstract class GenericDAOImpl<T extends Serializable> implements GenericDAO<T> {

    @PersistenceContext
    private EntityManager em;

    private Class<T> clazz;

    public EntityManager getEm() {
        return em;
    }

    @SuppressWarnings("unchecked")
    public GenericDAOImpl() {
        super();
        clazz = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    @Transactional
    public void create(final T entity) {
        em.persist(entity);
    }


    @Override
    @SuppressWarnings("unchecked")
    @Transactional
    public List<T> findAll() {
        Query query = em.createQuery("from " + this.clazz.getName());
        return query.getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional
    public List<T> findAllOrderByAsc(String column) {
        Query query = em.createQuery("from " + this.clazz.getName() + " order by " + column + " asc");
        return query.getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional
    public T getById(final Long id) {
        Query query = em.createQuery("from " + this.clazz.getName()
                + " where id=:id");
        query.setParameter("id", id);

        try {
            return (T) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    @Transactional
    public void update(final T entity) {
        em.merge(entity);
    }

    @Override
    @Transactional
    public void delete(final T entity) {
        em.remove(em.contains(entity) ? entity : em.merge(entity));
    }
}
