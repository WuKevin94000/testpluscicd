package fr.nexworld.demo.dao;

import fr.nexworld.demo.model.Sinister;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SinisterDAO extends GenericDAO<Sinister> {

    List<Sinister> findAll();

    List<Sinister> findByClientId(Long clientId);

    List<Sinister> getAllPendingSinister();

}
