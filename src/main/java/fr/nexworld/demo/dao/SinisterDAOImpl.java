package fr.nexworld.demo.dao;

import fr.nexworld.demo.model.Sinister;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class SinisterDAOImpl extends GenericDAOImpl<Sinister> implements SinisterDAO {

    @Override
    public List<Sinister> findAll() {
        TypedQuery<Sinister> query = getEm().createQuery(
                "select sinister "
                        + "from Sinister sinister"
                        + "ORDER BY CASE WHEN sinister.status = 'SUBMITTED' THEN 0 ELSE 1 END, sinister.date,"
                ,
                Sinister.class);
        return query.getResultList();
    }

    public List<Sinister> findByClientId(Long clientId) {
        TypedQuery<Sinister> query = getEm().createQuery(
                "select sinister "
                        + "from Sinister sinister "
                        + "join fetch sinister.contract contract "
                        + "where contract.clientId=:clientId "
                        + "ORDER BY CASE WHEN sinister.status = 'SUBMITTED' THEN 0 ELSE 1 END, sinister.date"
                ,
                Sinister.class);
        query.setParameter("clientId", clientId);
        return query.getResultList();
    }

    @Override
    public List<Sinister> getAllPendingSinister() {
        TypedQuery<Sinister> query = getEm().createQuery(
                "select sinister "
                        + "from Sinister sinister "
                        + "where sinister.status like 'SUBMITTED'"
                ,
                Sinister.class);
        return query.getResultList();
    }

}
