package fr.nexworld.demo.dao;

import fr.nexworld.demo.model.Contract;
import org.springframework.stereotype.Repository;

@Repository
public interface ContractDAO extends GenericDAO<Contract> {

}
