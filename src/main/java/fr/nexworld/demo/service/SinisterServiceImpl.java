package fr.nexworld.demo.service;

import fr.nexworld.demo.dao.SinisterDAO;
import fr.nexworld.demo.model.ContractType;
import fr.nexworld.demo.model.Sinister;
import fr.nexworld.demo.model.SinisterType;
import fr.nexworld.demo.service.rabbitmq.MessageProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SinisterServiceImpl implements SinisterService {

    @Autowired
    private SinisterDAO sinisterDAO;

    @Autowired
    MessageProducer messageProducer;

    @Override
    public List<Sinister> findAll() {
        return sinisterDAO.findAllOrderByAsc("id");
    }

    @Override
    public Sinister findById(Long id) {
        return sinisterDAO.getById(id);
    }

    @Override
    public List<Sinister> findByClientId(Long clientId) {
        return sinisterDAO.findByClientId(clientId);
    }

    @Override
    public List<Sinister> getAllPendingSinister() {
        return sinisterDAO.getAllPendingSinister();
    }

    @Override
    public void addSinister(Sinister sinister) {
        sinisterDAO.create(sinister);
        messageProducer.sendCreateMessage(sinister);
    }

    @Override
    public void editSinister(Sinister sinister) {
        sinisterDAO.update(sinister);
        messageProducer.sendUpdateMessage(sinister);
    }

    @Override
    public void deleteSinister(Sinister sinister) {
        messageProducer.sendDeleteMessage(sinister);
        sinisterDAO.delete(sinister);
    }

    @Override
    public List<SinisterType> getSinisterTypesByContractType(ContractType contractType) {
        return ContractType.getSinisterTypesByContractType(contractType);
    }

}
