package fr.nexworld.demo.service;

import fr.nexworld.demo.model.ContractType;
import fr.nexworld.demo.model.Sinister;
import fr.nexworld.demo.model.SinisterType;

import java.util.List;

public interface SinisterService {

    List<Sinister> findAll();

    Sinister findById(Long id);

    List<Sinister> findByClientId(Long clientId);

    List<Sinister> getAllPendingSinister();

    void addSinister(Sinister sinister);

    void editSinister(Sinister client);

    void deleteSinister(Sinister client);

    List<SinisterType> getSinisterTypesByContractType(ContractType contractType);
}
