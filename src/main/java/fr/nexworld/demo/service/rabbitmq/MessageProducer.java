package fr.nexworld.demo.service.rabbitmq;

import fr.nexworld.demo.model.Sinister;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class MessageProducer {

    private Logger logger = LoggerFactory.getLogger("RABBITMQ");

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private FanoutExchange fanoutCreate;

    @Autowired
    private FanoutExchange fanoutUpdate;

    @Autowired
    private FanoutExchange fanoutDelete;

    @Autowired
    Environment env;

    public void sendCreateMessage(Sinister sinister) {
        if (isProdEnvironment()) {
            convertAndSendSinister(sinister, fanoutCreate.getName());
        }
    }

    public void sendUpdateMessage(Sinister sinister) {
        if (isProdEnvironment()) {
            convertAndSendSinister(sinister, fanoutUpdate.getName());
        }
    }

    public void sendDeleteMessage(Sinister sinister) {
        if (isProdEnvironment()) {
            convertAndSendSinister(sinister, fanoutDelete.getName());
        }
    }

    private boolean isProdEnvironment() {
        String[] profiles = env.getActiveProfiles();
        return profiles.length == 0 || ! profiles[0].equals("test");
    }

    private void convertAndSendSinister(Sinister sinister, String fanoutName) {
        JSONObject jsonDeletedSinister = new JSONObject(sinister);
        String message = jsonDeletedSinister.toString();
        String logMessage = String.format("Sending message '%s' in fanout exchange %s", message, fanoutName);
        logger.info(logMessage);
        rabbitTemplate.convertAndSend(fanoutName, "", message);
    }

}

