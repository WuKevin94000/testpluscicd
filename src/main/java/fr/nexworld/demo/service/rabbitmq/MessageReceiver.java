package fr.nexworld.demo.service.rabbitmq;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import fr.nexworld.demo.dao.ContractDAO;
import fr.nexworld.demo.model.Contract;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

public class MessageReceiver {

    @Autowired
    ContractDAO contractDao;

    private Logger logger = LoggerFactory.getLogger("RABBITMQ");

    @RabbitListener(queues = "#{autoUpdateContractQueue.name}")
    public void receiveContractUpdate(String message) {
        JSONObject jsonDocument = new JSONObject(message);
        synchronizeDocument("update", jsonDocument);
    }

    @RabbitListener(queues = "#{autoCreateContractQueue.name}")
    public void receiveContractCreate(String message) {
        JSONObject jsonDocument = new JSONObject(message);
        synchronizeDocument("create", jsonDocument);
    }

    @RabbitListener(queues = "#{autoDeleteContractQueue.name}")
    public void receiveContractDelete(String message) {
        JSONObject jsonDocument = new JSONObject(message);
        synchronizeDocument("delete", jsonDocument);
    }

    private void synchronizeDocument(String operation, JSONObject json) {
        try {
            Gson gson = new GsonBuilder().create();
            logger.info("Contract received : ", json.toString());
            Contract documentObject = gson.fromJson(json.toString(), Contract.class);

            switch (operation) {
                case "create":
                    contractDao.create(documentObject);
                    logger.info("Contract created");
                    break;
                case "update":
                    contractDao.update(documentObject);
                    logger.info("Contract updated");
                    break;
                case "delete":
                    contractDao.delete(documentObject);
                    logger.info("Contract deleted");
                    break;
            }
        } catch (JSONException e) {
            logger.info("Project reference is null");
        } catch (JsonSyntaxException e) {
            logger.info("Invalid object syntax");
        }
    }


}
