package fr.nexworld.demo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Sinister implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    @Enumerated(EnumType.STRING)
    private SinisterType type;
    @Enumerated(EnumType.STRING)
    private SinisterStatus status;
    private String description;
    @ManyToOne
    @JoinColumn(name = "contractid")
    private Contract contract;
    private Long cost;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat( pattern = "dd/MM/yyyy" )
    private Date date;


    public void updatePartially(Sinister sinister) {
        if (sinister.getType() != null) this.setType(sinister.getType());
        if (sinister.getStatus() != null) this.setStatus(sinister.getStatus());
        if (sinister.getDescription() != null) this.setDescription(sinister.getDescription());
        if (sinister.getContract() != null) this.setContract(sinister.getContract());
        if (sinister.getCost() != null) this.setCost(sinister.getCost());
        if (sinister.getDate() != null) this.setDate(sinister.getDate());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SinisterType getType() {
        return type;
    }

    public void setType(SinisterType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SinisterStatus getStatus() {
        return status;
    }

    public void setStatus(SinisterStatus status) {
        this.status = status;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }
}
