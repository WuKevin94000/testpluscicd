package fr.nexworld.demo.model;

public enum SinisterStatus {
    SUBMITTED("Soumis"),
    ACCEPTED("Accepté"),
    DENIED("Refusé");

    private String text;

    private SinisterStatus(String text) {
        this.text = text;
    }

    public SinisterStatus getEnum(String text) {
        if (text == null) {
            return null;
        }
        for (SinisterStatus type : values()) {
            if (type.getText().equals(text)) {
                return type;
            }
        }
        return null;
    }

    public String getText() {
        return text;
    }
}
