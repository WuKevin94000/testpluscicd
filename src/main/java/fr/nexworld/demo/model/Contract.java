package fr.nexworld.demo.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Contract implements Serializable {

    @Id
    private Long id;
    private Long clientId;
    @Enumerated(EnumType.STRING)
    private ContractType type;

    public Long getId() {
        return id;
    }

    public Long getClientId() {
        return clientId;
    }

    public ContractType getType() {
        return type;
    }
}
