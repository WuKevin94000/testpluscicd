package fr.nexworld.demo.model;

public enum SinisterType {
    INCENDIE("Incendie"),
    DECES("Décès"),
    DEGAT_DES_EAUX("Dégats des eaux"),
    PERTE_EXPLOITATION("Perte d'exploitation"),
    ACCIDENT_VEHICULE("Accident véhiculé"),
    MEDICAL("Medical"),
    VOL("Vol"),
    CATASTROPHES_NATURELLES("Catastrophes naturelles");

    private String text;

    private SinisterType(String text) {
        this.text = text;
    }

    public SinisterType getEnum(String text) {
        if (text == null) {
            return null;
        }
        for (SinisterType type : values()) {
            if (type.getText().equals(text)) {
                return type;
            }
        }
        return null;
    }

    public String getText() {
        return text;
    }
}