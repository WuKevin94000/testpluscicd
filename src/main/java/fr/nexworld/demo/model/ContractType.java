package fr.nexworld.demo.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum ContractType {
    VIE (SinisterType.DECES),
    MALADIE (SinisterType.MEDICAL),
    PERTE_EXPLOITATION (SinisterType.PERTE_EXPLOITATION),
    TIERS (SinisterType.ACCIDENT_VEHICULE),
    BIENS (SinisterType.INCENDIE, SinisterType.DEGAT_DES_EAUX, SinisterType.CATASTROPHES_NATURELLES);

    ContractType(SinisterType... types) {
        this.types = types;
    }

    private SinisterType[] types;

    public static List<SinisterType> getSinisterTypesByContractType(ContractType contractType) {
        ArrayList<SinisterType> sinisters = new ArrayList<>();
        if (contractType == null) {
            return sinisters;
        }
        for (ContractType contractValue : values()) {
            if (contractValue.equals(contractType)) {
                sinisters.addAll(Arrays.asList(contractValue.getSinisterTypes()));
            }
        }
        return sinisters;
    }

    public SinisterType[] getSinisterTypes() {
        return types;
    }
}
