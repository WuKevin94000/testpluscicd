package fr.nexworld.demo.controller;

import fr.nexworld.demo.exception.NotFoundException;
import fr.nexworld.demo.model.ContractType;
import fr.nexworld.demo.model.Sinister;
import fr.nexworld.demo.model.SinisterStatus;
import fr.nexworld.demo.model.SinisterType;
import fr.nexworld.demo.service.SinisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(value = "services/sinister")
public class SinisterController {

    @Autowired
    SinisterService sinisterService;

    @GetMapping
    @ResponseBody
    public List<Sinister> getAllSinister() {
        return sinisterService.findAll();
    }

    @GetMapping(value = "/status")
    @ResponseBody
    public List<SinisterStatus> getAllSinisterStatus() {
        return Arrays.asList(SinisterStatus.values());
    }

    @GetMapping(value = "/types")
    @ResponseBody
    public List<SinisterType> getAllSinisterType() {
        return Arrays.asList(SinisterType.values());
    }

    @GetMapping(value = "/{id}")
    public Sinister getSinisterById(@PathVariable("id") Long id) {
        Sinister sinister = sinisterService.findById(id);
        if (sinister == null) {
            throw new NotFoundException();
        }
        return sinister;
    }

    @GetMapping(value = "/client/{id}")
    public List<Sinister> getSinisterByClientId(@PathVariable("id") Long clientId) {
        List<Sinister> sinisterList = sinisterService.findByClientId(clientId);
        if (sinisterList == null) {
            throw new NotFoundException();
        }
        return sinisterList;
    }

    @GetMapping(value = "/pending")
    public List<Sinister> getAllPendingSinister() {
        List<Sinister> sinisterList = sinisterService.getAllPendingSinister();
        if (sinisterList == null) {
            throw new NotFoundException();
        }
        return sinisterList;
    }

    @GetMapping(value = "/types/contractType/{type}")
    public List<SinisterType> getSinisterTypesByContractType(@PathVariable("type") ContractType contractType) {
        return this.sinisterService.getSinisterTypesByContractType(contractType);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Sinister addSinister(@RequestBody Sinister sinister) {
        sinisterService.addSinister(sinister);
        return sinister;
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Sinister editSinister(@PathVariable("id") Long id, @RequestBody Sinister sinister) {
        Sinister sinisterToUpdate = sinisterService.findById(id);
        sinisterToUpdate.updatePartially(sinister);
        sinisterService.editSinister(sinisterToUpdate);
        return sinisterToUpdate;
    }

    @DeleteMapping(value = "/{id}")
    public void deleteSinister(@PathVariable("id") Long id) {
        Sinister sinister = sinisterService.findById(id);
        if (sinister == null) {
            throw new NotFoundException();
        }
        sinisterService.deleteSinister(sinister);
    }
}
