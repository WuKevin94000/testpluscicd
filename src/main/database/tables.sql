CREATE TABLE contract (
    id              integer,
    clientid        integer,
    type            text
);

CREATE TABLE sinister (
    id              SERIAL PRIMARY KEY,
    type            text,
    status          text,
    description     text,
    contractid      int,
    cost            int,
    date            date
);