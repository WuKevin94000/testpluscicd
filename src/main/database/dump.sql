INSERT INTO contract(id, clientid, type) VALUES
    (1, 2, 'BIENS'),
    (2, 2, 'MALADIE'),
    (3, 1, 'PERTE_EXPLOITATION');


INSERT INTO sinister(type, status, description, contractid, cost, date) VALUES
('VOL', 'ACCEPTED', 'Vol de téléphone portable', 3, 250, '2019-05-09'),
('INCENDIE', 'ACCEPTED', 'Incendie criminel', 3, 250, '2019-05-09');