#!/bin/sh

LATEST_TAG="$(git describe --tags --abbrev=0 --match "[0-9]*.[0-9]*.[0-9]*" 2>/dev/null)"

BREAKING_CHANGES=""

FEATURES=""
BUG_FIXES=""
CHORES=""
DOCS=""
STYLES=""
REFACTORS=""
PERFS=""
TESTS=""
CIS=""
IMPROVEMENTS=""

if [ "$LATEST_TAG" ]; then
  OLD_VERSION="$(echo $LATEST_TAG | grep -E '([0-9]+\.){2}[0-9]+')"
  OLD_MAJOR="$(echo "$OLD_VERSION" | cut -d'.' -f1)"
  OLD_FEATURE="$(echo "$OLD_VERSION" | cut -d'.' -f2)"
  OLD_PATCH="$(echo "$OLD_VERSION" | cut -d'.' -f3)"
  LATEST_TAG="$LATEST_TAG.."
else
  OLD_MAJOR=0
  OLD_FEATURE=0
  OLD_PATCH=0
fi

for i in $(git rev-list ${LATEST_TAG}HEAD); do
  SUBJECT="$(git show -s --pretty=%s $i)"
  BODY="$(git show -s --pretty=%b $i)"

  SUBJECT_TYPE="$(echo "$SUBJECT" | cut -sd':' -f1)"

  SCOPE="$(echo "$SUBJECT" | cut -sd'(' -f2 | cut -sd')' -f1)"
  [ "$SCOPE" ] && SCOPE=" **$SCOPE**:"
  SUBJECT_TITLE=$(echo "$SUBJECT" | cut -d':' -f2)
  COMMIT=$(echo "$i" | cut -c1-7)

  case "$BODY" in
    *BREAKING?CHANGE*)
      MAJOR=1
      BODY="$(echo $BODY | awk -F'BREAKING CHANGE' '{print $2;}' | grep -E "[[:alnum:]].*")"
      BREAKING_CHANGES="${BREAKING_CHANGES}-${SCOPE}$BODY\n"
      ;;
  esac

  case "$SUBJECT_TYPE" in
    *fix*)
      PATCH=1
      BUG_FIXES="${BUG_FIXES}-$SCOPE$SUBJECT_TITLE ($COMMIT)\n"
      ;;
    *feat*)
      FEATURE=1
      FEATURES="${FEATURES}-$SCOPE$SUBJECT_TITLE ($COMMIT)\n"
      ;;
    *chore*)
      CHORES="${CHORES}-$SCOPE$SUBJECT_TITLE ($COMMIT)\n"
      ;;
    *doc*)
      DOCS="${DOCS}-$SCOPE$SUBJECT_TITLE ($COMMIT)\n"
      ;;
    *style*)
      STYLES="${STYLES}-$SCOPE$SUBJECT_TITLE ($COMMIT)\n"
      ;;
    *refactor*)
      REFACTORS="${REFACTORS}-$SCOPE$SUBJECT_TITLE ($COMMIT)\n"
      ;;
    *perf*)
      PERFS="${PERFS}-$SCOPE$SUBJECT_TITLE ($COMMIT)\n"
      ;;
    *test*)
      TESTS="${TESTS}-$SCOPE$SUBJECT_TITLE ($COMMIT)\n"
      ;;
    *ci*)
      CIS="${CIS}-$SCOPE$SUBJECT_TITLE ($COMMIT)\n"
      ;;
    *improvement*)
      IMPROVEMENTS="${IMPROVEMENTS}-${SCOPE}$SUBJECT_TITLE ($COMMIT)\n"
      ;;
  esac
done

[ "$FEATURES" ] && RELEASE_NOTES="# Features\n$FEATURES\n"
[ "$BUG_FIXES" ] && RELEASE_NOTES="${RELEASE_NOTES}\n# Bug fixes\n$BUG_FIXES\n"
[ "$CHORES" ] && RELEASE_NOTES="${RELEASE_NOTES}# Chores\n$CHORES\n"
[ "$DOCS" ] && RELEASE_NOTES="${RELEASE_NOTES}# Documentation\n$DOCS\n"
[ "$STYLES" ] && RELEASE_NOTES="${RELEASE_NOTES}# Styles\n$STYLES\n"
[ "$REFACTORS" ] && RELEASE_NOTES="${RELEASE_NOTES}# Refactors\n$REFACTORS\n"
[ "$PERFS" ] && RELEASE_NOTES="${RELEASE_NOTES}# Performance\n$PERFS\n"
[ "$TESTS" ] && RELEASE_NOTES="${RELEASE_NOTES}# Tests\n$TESTS\n"
[ "$CIS" ] && RELEASE_NOTES="${RELEASE_NOTES}# Ci\n$CIS\n"
[ "$IMPROVEMENTS" ] && RELEASE_NOTES="${RELEASE_NOTES}# Improvements\n$IMPROVEMENTS\n"
[ "$BREAKING_CHANGES" ] && RELEASE_NOTES="${RELEASE_NOTES}# BREAKING CHANGES\n$BREAKING_CHANGES\n"

ORIGIN="$(git config --get remote.origin.url | cut -d'@' -f2)"
GIT_URL="$(echo $ORIGIN | cut -sd':' -f1)"

if [ "$GIT_URL" ]; then
  GIT_USER="$(echo $ORIGIN | cut -d':' -f2 | awk 'BEGIN{FS=OFS="/"}{NF--; print}')"
  GIT_REPO="$(echo $ORIGIN | awk -F '/' '{printf $NF}' | awk -F'.git' '{print $1}')"
else
  GIT_URL="$(echo $ORIGIN | cut -sd'/' -f1)"
  GIT_USER="$(echo $ORIGIN \
    | awk 'BEGIN{FS="/"}{for (i=2; i < NF - 1; i++) printf $i "/"; print $(NF-1)}')"
  GIT_REPO="$(echo $ORIGIN | awk -F '/' '{printf $NF}' | awk -F'.git' '{print $1}')"
fi

if [ "$MAJOR" ]; then
  MAJOR=$((OLD_MAJOR + 1))
  FEATURE=0
  PATCH=0
elif [ "$FEATURE" ]; then
  MAJOR=$OLD_MAJOR
  FEATURE=$((OLD_FEATURE + 1))
  PATCH=0
elif [ "$PATCH" ]; then
  MAJOR=$OLD_MAJOR
  FEATURE=$OLD_FEATURE
  PATCH=$((OLD_PATCH + 1))
else
  printf "$RELEASE_NOTES"
  echo "No release"
  exit
fi

NEW_TAG="$MAJOR.$FEATURE.$PATCH"
HEAD="$(git rev-parse HEAD)"

if [ "$GITLAB_TOKEN" ]; then
  curl -sH 'Content-Type: application/json' -H "PRIVATE-TOKEN: $GITLAB_TOKEN" \
    --data "{ \"name\": \"$NEW_TAG\", \"tag_name\": \"$NEW_TAG\", \"ref\": \"$HEAD\", \"description\": \"$RELEASE_NOTES\"}" \
    --request POST "https://$GIT_URL/api/v4/projects/$GIT_USER%2F$GIT_REPO/releases"
else
  echo "Environment variable GITLAB_TOKEN not found"
  exit 1
fi
